

const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    totalPrice: {
        type: Number,
    },
    products: [
        {
            productId: {
                type: String,
                required: [true, "productId is required."]
            },
            productName: {
                type: String,
                required: [true, "Product Name is required"]
            },
            sizeOrder: {
                type: String,
                required:[true, "Size is required"]
            },
            quantity:{
                type: Number,
                required: [true, "quantity is required."],
                default: 1
            },
            totalPerProduct: {
				type: Number
			},
            purchasedOn: {
                type: Date,
                default: new Date()
            }

        }
    ],
    userId:{
        type: String,
        required:[true, "userId is required"]
    }
   
});

module.exports = mongoose.model("Order", orderSchema);
