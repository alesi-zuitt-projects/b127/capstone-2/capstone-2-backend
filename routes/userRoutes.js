const express = require('express');
const router = express.Router();
const userController = require ('../controllers/userControllers');
const auth = require ('../auth');


//Route for checking if the user's email already exists in the database
router.post('/checkEmail', (req, res) =>{
	userController.checkEmailExists(req.body).then(result => res.send(result));
})

router.get('/details', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    userController.getProfile({_id: userData.id}).then(result => res.send(result));
})




//user registration
router.post('/registration', (req,res) => {
	userController.registerUser(req.body).then(result => res.send(result))
})

//Login User
router.post('/loginUser', (req, res) =>{
	userController.loginUser(req.body).then(result => res.send(result));
})


//getAllProducts
router.get('/products', (req,res) => {
	userController.getAllProducts().then(result => res.send(result))
})


//get specific order
router.get("/:userId/orderlists", auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		
	}
	if(data){
		userController.getUserOrders(data).then(result => res.send(result))
	}else{
		res.send("please authenticate")
	}
})

module.exports = router;