
const Admin = require ('../models/Admin');
const User = require('../models/User');
const Shoe = require('../models/Product')
const bcrypt = require('bcrypt');
const auth = require('../auth');

//admin reggistration
module.exports.registerAdmin = (reqBody) => {
	return Admin.find (	{username: reqBody.username }	).then((result) => {
		if (result.length > 0) {
			return "username already exist!"
		}else{
			let newAdmin = new Admin ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		username: reqBody.username,
		password: bcrypt.hashSync(reqBody.password, 10),
		address: reqBody.address
		})

		//save
		return newAdmin.save().then((user, error) => {
		//error
		if(error){
			return false;
		}else {
			return user;
		}
		})
	}
})
};

//loginAdmin
module.exports.loginAdmin = (reqBody) => {
	return Admin.findOne({username: reqBody.username}).then(result => {
		 if (result == null){
			return "not registered!!!";
		}else {
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect){
				
				return {	accessToken: auth.createAccessToken(result.toObject())	}
			}else{
				
				return false;
			}
		}
	})
};

//getAdmins
module.exports.getAllAdmin = () =>{
	return Admin.find().then(result => {
		return result;
	})
}

//getUsers from userRoutes and userController connected to admin
module.exports.getAllUser = () => {
	return User.find().then(result => {
		return result
	})
}

//set user as an admin
module.exports.setAdmin = (reqParams) => {
	let updatedUser = {
		isAdmin: true
	}
	return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((result, error) =>{
		if(error){
			return ("Please enter a valid userId")
		}else{
			return result
		}
	})
}

//getUserdetailsviaId
// details by id
module.exports.getUser = (reqParams) => {
	return User.findById(reqParams).then(result => {
		return result;
	})
}

//get all orders
module.exports.getAllOrder= () => {
	
	return User.find({isAdmin: false}).then(result => {
		return result;
	})
}

//deleteuser
module.exports.deleteUser = (reqParams) => {
	
		return User.findByIdAndRemove(reqParams).then((removedUser,err) =>{
			if(err){
				return false;
			}else{
				return ("successfully deleted!!!");
			}
		})
	}
//delete product
module.exports.deleteProduct = (reqParams) => {
	return Shoe.findByIdAndRemove(reqParams).then ((removedProduct, err) => {
		if(err){
			return false;
		}else{
			return ("successfully delete!!!");
		}
	})
}


