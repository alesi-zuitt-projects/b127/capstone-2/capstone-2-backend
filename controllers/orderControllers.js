const Cart = require('../models/Cart');
const Order = require('../models/Order');


module.exports.OrderNow = async (data) => {
	
	let isOrderSaved = await Cart.findOne({userId: data.userId}).then(result => {

		if (result !== null) {
			let newOrder = new Order({
				totalPrice: result.totalPrice,
				products: result.products,
				userId: data.userId
			})
	
			return newOrder.save().then((result,isError)=> {
				return(isError)? false: true;
			})
		} else {
			return false;
		}
	})

	let isCartDeleted = await Cart.findOneAndRemove({userId: data.userId}).then((result,isError) => {
		return (result !== null)? true : false;
	})

	if (isOrderSaved && isCartDeleted) {
		return true;
	} else {
		return false;
	}
}

module.exports.getAllOrdersProto = () => {
	return Order.find({}).then(result => {
		return result
	})
}


/*module.exports.myOrdersProto = (data) => {
	return Order.find({userId: data.userId}).then(result =>{
		let productArr=[];
		result.forEach(item=>{
			productArr.push(item.products)
		})
		return productArr.flat();
	});
}*/



module.exports.getMyOrders = (data) => {
	return Order.find({userId: data.userId}).then(result =>{
		let orderArr	=	[];
		result.forEach(order	=>	{
			orderArr.push(order.products)
		})
		return orderArr.flat();
	});
}